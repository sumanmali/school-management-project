@extends('layouts.menu')

@section('content')
<!-- bradcame area  -->
<div class="bradcam-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title white-title bradcam-title text-uppercase text-center">
                   <h2> Events </h2>
                   <span class="star"></span>
                   <span class="star"></span>
                   <span class="star"></span>
               </div>
           </div>
           <div class="bradcam-wrap text-center">
            <nav class="bradcam-inner">
              <a class="bradcam-item text-uppercase" href="https://demo.inilabs.net/school/v4.2/frontend/page/home">Home</a>
              <span class="brd-separetor">/</span>
              <span class="bradcam-item active text-uppercase">Events</span>
          </nav>
      </div>
  </div>
</div>
</div>
<!-- bradcame area  -->


<section id="events" class="events-area area-padding">
    <div class="container">
        <div class="single-event-list">
            <div class="event-img">
                <a href="https://demo.inilabs.net/school/v4.2/frontend/event/4"><img src="https://demo.inilabs.net/school/v4.2/uploads/images/444014202f6bd712d21a2326494f01157e5fa98cffe85c5ff6ff7215b59662614a1668edee7026a8c3952fa2a030b8278058e594c4db5cc53e3eac03f1c79702.png" alt=""></a>
            </div>
            <div class="event-content">
                <div class="event-meta">
                    <div class="event-date first-date">
                        10<span>Jun</span>
                    </div>
                    <div class="event-date second-date">
                        20<span>Jun</span>
                    </div>

                    <div class="event-info">
                        <h4><a href="">HACKATHON</a></h4>
                        <div class="event-time">
                            <span class="event-title">Time: </span>
                            <span>12:00 AM - 11:00 PM</span>
                        </div>
                    </div>
                </div>
                <a id="4" href="#" class="primary-btn style--two going-event">Going now</a>
            </div>
        </div>
    </div><div class="single-event-list">
        <div class="event-img">
            <a href="https://demo.inilabs.net/school/v4.2/frontend/event/1"><img src="https://demo.inilabs.net/school/v4.2/uploads/images/048a860d45ce0126456925624f7d0219cfc5eebd9a3f913d60821855444098b4e64b276595f35cca85f42692bc8693ec98a5ea7168b926483c6d76c5ee2b0364.jpg" alt=""></a>
        </div>
        <div class="event-content">
            <div class="event-meta">
                <div class="event-date first-date">
                    05<span>Mar</span>
                </div>
                <div class="event-date second-date">
                    10<span>Mar</span>
                </div>

                <div class="event-info">
                    <h4><a href="https://demo.inilabs.net/school/v4.2/frontend/event/1">Programming Contest</a></h4>
                    <div class="event-time">
                        <span class="event-title">Time: </span>
                        <span>12:00 AM - 11:00 PM</span>
                    </div>
                </div>
            </div>
            <a id="1" href="#" class="primary-btn style--two going-event">Going now</a>
        </div>
    </div>
</div>
<div class="col-md-4 col-sm-6">

    <div class="single-event-list">
        <div class="event-img">
            <a href="https://demo.inilabs.net/school/v4.2/frontend/event/5"><img src="https://demo.inilabs.net/school/v4.2/uploads/images/caa2bb57991a4e6c11d94030eae7f2fe0c3707ce0afba39f0d84fab23f788bbd6f0736f8bae5c493fd373508439b29d450ba16ab01ec7ca2c65dec674c9342df.JPG" alt=""></a>
        </div>
        <div class="event-content">
            <div class="event-meta">
                <div class="event-date first-date">
                    21<span>Feb</span>
                </div>

                <div class="event-info">
                    <h4><a href="https://demo.inilabs.net/school/v4.2/frontend/event/5">International Language Day</a></h4>
                    <div class="event-time">
                        <span class="event-title">Time: </span>
                        <span>12:00 AM - 11:00 PM</span>
                    </div>
                </div>
            </div>
            <a id="5" href="#" class="primary-btn style--two going-event">Going now</a>
        </div>
    </div>
</div>
</div>
<div class="single-event-list">
    <div class="event-img">
        <a href="https://demo.inilabs.net/school/v4.2/frontend/event/2"><img src="https://demo.inilabs.net/school/v4.2/uploads/images/8d099c61dc71fd623ac1ab28302cf98798d981c3dbadc91836ba373580fb0f4d26c3b5311bca4c9b19dc98ccfc13f851eab04e3057a171b001bc9ce331e28eda.jpg" alt=""></a>
    </div>
    <div class="event-content">
        <div class="event-meta">
            <div class="event-date first-date">
                27 <span>Jan</span>
            </div>

            <div class="event-info">
                <h4><a href="https://demo.inilabs.net/school/v4.2/frontend/event/2">Cricket Tournament</a></h4>
                <div class="event-time">
                    <span class="event-title">Time: </span>
                    <span>12:00 AM - 11:59 PM</span>
                </div>
            </div>
        </div>
        <a id="2" href="#" class="primary-btn style--two going-event">Going now</a>
    </div>
</div>
</div>
<div class="single-event-list">
    <div class="event-img">
        <a href="https://demo.inilabs.net/school/v4.2/frontend/event/3"><img src="https://demo.inilabs.net/school/v4.2/uploads/images/94581e07babcbf7e56e6e7fdae5f612f783ff34465794c5bea654b620d49e704ae05cda23b52af8dd92abc89220b1517e7fcc89136ec07a14fcd7f3b15a6cb87.png" alt=""></a>
    </div>
    <div class="event-content">
        <div class="event-meta">
            <div class="event-date first-date">
                27<span>Jan</span>
            </div>

            <div class="event-info">
                <h4><a href="https://demo.inilabs.net/school/v4.2/frontend/event/3">Football tournament</a></h4>
                <div class="event-time">
                    <span class="event-title">Time: </span>
                    <span>12:00 AM - 11:59 PM</span>
                </div>
            </div>
        </div>
        <a id="3" href="#" class="primary-btn style--two going-event">Going now</a>
    </div>
</div>
</div>
</div>
</section>

<!-- Start About Content -->
<section id="about" class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-about">
                    <p>  </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Close About Content -->
@endsection