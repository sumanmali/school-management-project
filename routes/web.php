<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','MainController@index');

Route::get('/about', function () {
    return view('frontweb/about');
});
Route::get('/noticeboard', function () {
    return view('frontweb/noticeboard');
});
Route::get('/teachers', function () {
    return view('frontweb/teacher');
});
Route::get('/events', function () {
    return view('frontweb/events');
});
Route::get('/gallery', function () {
    return view('frontweb/gallery');
});
Route::get('/audio-list', function () {
    return view('frontweb/audio');
});
Route::get('/video-list', function () {
    return view('frontweb/video');
});
Route::get('/contact', function () {
    return view('frontweb/contact');
});
Route::get('/blog', function () {
    return view('frontweb/blog');
});
Route::get('/admission', function () {
    return view('frontweb/admission');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/teacher', 'TeacherController@index');
        Route::get('/teacher/create', 'TeacherController@create');
        Route::post('/teacher/create', 'TeacherController@store');
        Route::get('/teacher/edit/{id}', 'TeacherController@edit');
        Route::post('/teacher/edit/{id}', 'TeacherController@update');
        Route::get('/teacher/delete/{id}', 'TeacherController@destroy');



Route::get('/home/slider/index', 'SliderController@index');
Route::get('/home/slider/create', 'SliderController@create');
Route::post('/home/slider/create', 'SliderController@store');
Route::get('/home/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/home/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/home/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');

Route::get('/home/work/index', 'WorkController@index');
Route::get('/home/work/create', 'WorkController@create');
Route::post('/home/work/create', 'WorkController@store');
Route::get('/home/work/edit/{id}', 'WorkController@edit')->name('work.edit');
Route::post('/home/work/edit/{id}', 'WorkController@update')->name('work.update');
Route::delete('/home/work/destroy/{id}', 'WorkController@destroy')->name('work.delete');


        Route::get('/subject', 'SubjectController@index');
        Route::get('/subject/create', 'SubjectController@create');
        Route::post('/subject/create', 'SubjectController@store');
        Route::get('/subject/edit/{id}', 'SubjectController@edit');
        Route::post('/subject/edit/{id}', 'SubjectController@update');
        Route::get('/subject/delete/{id}', 'SubjectController@destroy');



             Route::get('/user', 'UserController@index');
        Route::get('/user/edit/{id}', 'UserController@edit');
        Route::post('/user/edit/{id}', 'UserController@update');
        Route::get('/user/delete/{id}', 'UserController@destroy');

