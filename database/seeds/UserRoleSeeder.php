<?php

use Illuminate\Database\Seeder;
use App\Role;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new App\Role;
        $role_admin->name="Admin";
        $role_admin->save();

        $role_teacher = new App\Role;
        $role_teacher->name="Teacher";
        $role_teacher->save();

        $role_student = new App\Role;
        $role_student->name="Student";
        $role_student->save();

        $role_unassigned = new App\Role;
        $role_unassigned->name="Unassigned";
        $role_unassigned->save();
    }
}
