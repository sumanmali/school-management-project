@extends('layouts.admin')

@section('content')
<div class="container-fluid">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		<div class="col-sm-3 admin_sidebar">
			@include('layouts.sidebar')
		</div>
		<div class="col-sm-9 col-sm-8">
			<form method="post" action="/teacher/create" enctype="multipart/form-data">
			@csrf
				<h3>Add New Teacher</h3>
				<div class="form-group">
					<input type="hidden" value="{{csrf_token()}}" name="name"/>
					<label for="name">Teacher Name</label>
				<input class="form-control" type="text" name="name"/>
					<input type="hidden" value="{{csrf_token()}}" name="subject"/>
					<label for="subject">Subject</label>
					<select name="subject" class="form-control">
					<option value="">Choose Name</option>
					@foreach($subject as $s)
					<option value="{{$s->name}}">{{ $s->name}}</option>
					@endforeach
				</select>
				<div class="form-group">
				<input type="file" name="image" accept="image/jpg, image/png, image/jpeg">
				</div>
				</div>
				
				<div class="form-group">
					<button type="submit" class="form-control btn btn-primary btn-block">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection