@extends('layouts.admin')

@section('content')
<div class="container-fluid">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		<div class="col-sm-3 admin_sidebar">
			@include('layouts.sidebar')
		</div>
		<div class="col-sm-9 col-sm-8">
			<form method="post" action="/teacher/edit/{{$teacher->id}}" enctype="multipart/form-data">
			@csrf
				<h3>Edit teacher_info</h3>
				<div class="form-group">
					<input type="hidden" value="{{csrf_token()}}" name="name"/>
					<label for="name">name:
						</label>
					<input class="form-control" type="text" name="name" value='{{ $teacher->name }}'/>
					<input type="hidden" value="{{csrf_token()}}" name="subject"/>
					<label for="subject">Subject:</label>
					<input class="form-control" type="text" name="subject" value='{{$teacher->subject}}'/>
				</div>
				<div class="form-group">
					<input type="file" name="image" value="{{ asset('/images/'.$teacher->image )}}	" accept="images/png, images/jpg, images/jpeg">
					<img src="{{ asset('/images/'.$teacher->image )}}" style="width:150px;">
				</div>
				<div class="form-group">
					<button type="submit" class="form-control btn btn-primary btn-block">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection