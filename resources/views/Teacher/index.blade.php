@extends('layouts.admin')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3 admin_sidebar">
			@include('layouts.sidebar')
		</div>
		<div class="col-sm-9 col-sm-8">
			<h3>Teacher</h3>
			<a class="btn btn-primary" href="{{url('/teacher/create')}}">Create teacher</a>
			<table class="table table-hover table-teacher">
				<thead>
					<th>id </th>
					<th>name </th>
					<th>subject</th>
					<th>image</th>
					<th>Action</th>
				</thead>
				@foreach($teacher as $teachers)
				<tbody>
					<td>{{$teachers->id}}</td>
					<td>{{$teachers->name}}</td>
					<td>{{$teachers->subject}}</td>
					<td><img src="/images/{{$teachers->image}}" alt="{{$teachers->name}}" ></td>
					<td>
						<a class="btn btn-info btn-sm" href="/teacher/edit/{{$teachers->id}}">Edit</a><br>
						<a class="btn btn-danger btn-sm" href="/teacher/delete/{{$teachers->id}}">Delete</a>
					
					</td>

				</tbody>
				@endforeach
			</table>
		</div>
	</div>
</div>

@endsection