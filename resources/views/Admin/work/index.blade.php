@extends('layouts.admin')

@section('content')
<div class="row">
      <ol class="breadcrumb">
        <li><a href="/home">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Works</li>
      </ol>
    </div><!--/.row-->
<div class="col-sm-9">
					<div class="panel-heading">
						All Works
						</div>
						<a href="/home/work/create"><button class="btn btn-primary">Create new Work</button></a>
					<div class="panel-body">
			<table class="allWork">
			<div class="container">
			<thead>
				<tr><th>Id</th>
					<th>Name</th>
					<th>Description</th>
					<th>Image</th>
					<th>Action</th>
				</tr>
				</thead>
			<tbody>
				@foreach($works as $Work)
				<tr>
				<td>{{$Work->id}}</td>
				<td>{{$Work->name}}</td>
				<td>{{ $Work->description }}</td>
				<td class="work-img">
					<img src="/images/{{$Work->image}}" alt="{{$Work->name}}">
				</td>
				<td><a href="{{route('work.edit', $Work->id)}}"><button type="btn btn-primary">Edit</button></a>|
				<form method="post" action="{{route('work.delete',$Work->id)}}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" class="btn btn-danger">Delete</button>
				</form>
				</td>
			</tr>
				@endforeach
			</tbody>
		</div>
			</table>
					</div>
				</div>
			</div><!--/.col-->
@endsection