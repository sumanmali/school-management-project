@extends('layouts.admin')

@section('content')
<div class="row">
      <ol class="breadcrumb">
        <li><a href="/home">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Work</li>
      </ol>
    </div>
    
<div class="col-sm-9">
	@if($errors->any())
		@foreach($errors->all() as $error)
			<ul>
				<li>{{$error}}</li>
			</ul>
		@endforeach
	@endif
				<div class="panel-heading">
						Edit Work
						<a href="/home/work/index" class="pull-right panel-toggle bck-btn"><em class="fa fa-toggle-left">&nbsp;<b>Back</b></em></a>
					</div>
					<div class="panel-body">
						<form class="form-horizontal" action="/home/work/edit/{{ $works->id}}" method="post" enctype="multipart/form-data">
							@csrf
							<fieldset>
								<div class="form-group">
									<label class="col-md-3 control-label" for="name">Title:</label>
									<div class="col-md-9">
										<input id="name" name="name" type="text" value="{{$works->name}}" class="form-control">
									</div>
								</div>
							
								<div class="form-group">
									<label class="col-md-3 control-label" for="image">Description:</label>
									<div class="col-md-9">
										<textarea id="description" name="description" type="text" value="" class="form-control">{{ $works->description}}</textarea>
									</div>
								</div>
								<!-- image input-->
								<div class="form-group">
									<label class="col-md-3 control-label" for="image">Select Image:</label>
									<div class="col-md-9 edit-img">
										<input type="file" class="form-control" name="image"  accept="image/png, image/jpg, image/jpeg">
										<img src="/images/{{$works->image}}" alt="{{$works->name}}">
									</div>
								</div>
								
								<!-- Form actions -->
								<div class="form-group">
									<div class="col-md-12 widget-right">
										<button type="submit" class="btn btn-default btn-md pull-right">Submit</button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			<!--/.col-->
@endsection