@extends('layouts.admin')

@section('content')
<div class="row">
      <ol class="breadcrumb">
        <li><a href="/home">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Slider</li>
      </ol>
    </div><!--/.row-->
    
<div class="col-sm-9">
					<div class="panel-heading">
						All Sliders
						</div>
						<a href="/home/slider/create"><button class="btn btn-primary">Add New Slider</button></a>
					<div class="panel-body">
			<table class="allslider">
			<div class="container">
			<thead>
				<tr><th>Id</th>
					<th>Name</th>
					<th>Image</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
				</thead>
			<tbody>
				@foreach($sliders as $slider)
				<tr>
				<td>{{$slider->id}}</td>
				<td>{{$slider->name}}</td>
				<td>
					<img src="/images/{{$slider->image}}" alt="{{$slider->name}}">
				</td>
				<td>{{ $slider->description }}</td>
				<td><a href="{{route('slider.edit', $slider->id)}}"><button type="btn btn-primary">Edit</button></a>|
				<form method="post" action="{{route('slider.delete',$slider->id)}}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" class="btn btn-danger">Delete</button>
				</form>
				</td>
			</tr>
				@endforeach
			</tbody>
		</div>
			</table>
					</div>
				</div>
@endsection