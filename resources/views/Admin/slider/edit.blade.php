@extends('layouts.admin')

@section('content')
<div class="row">
      <ol class="breadcrumb">
        <li><a href="/home">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Slider</li>
      </ol>
    </div><!--/.row-->
    
<div class="col-sm-9">
					<div class="panel-heading">
						Edit Sliders
						<a href="/home/slider/index" class="pull-right panel-toggle bck-btn"><em class="fa fa-toggle-left">&nbsp;<b>Back</b></em></a>
					</div>
					<div class="panel-body">
	<div class="container my-create">
				@if($errors->any())
		@foreach($errors->all() as $error)
			<ul>
				<li>{{$error}}</li>
			</ul>
		@endforeach
	@endif
	<form method="POST" action="/home/slider/edit/{{ $slider->id }}" enctype="multipart/form-data">
	@csrf
	<ul style="list-style: none;">
	
	<li>
			<div class="form-group">
		<label for="name">
			Edit the slider name:
		</label>
		<input type="text" class="form-control" name="name" value='{{$slider->name}}' >
	</div>
	</li>
	<li>
	<div class="form-group">
		<label for="image">
			Select the image:
		</label>
		<input type="file" class="form-control" name="image" accept="image/png, image/jpg, image/jpeg">
		<img src="/images/{{$slider->image}}" alt="{{$slider->name}}" >																
	</div>
	</li>
	<li>
		<div class="form-group">
		<label class="control-label" for="description">Description:</label>
		<textarea name="description" type="text" required="" placeholder="Slider description" class="form-control">{{ $slider->description }}</textarea>
		</div>
	</li>
	<li>
		<div class="form-group">
			<button type="submit" class="btn btn-primary" value="update">Update project</button>
		</div>
	</li>
</ul>
</form>

				</div>
				</div>
			</div><!--/.col-->
		</div>
@endsection