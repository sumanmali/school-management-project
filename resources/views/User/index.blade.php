@extends('layouts.admin')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3 admin_sidebar">
			@include('layouts.sidebar')
		</div>
		<div class="col-sm-9 col-sm-8">
			<table class="table table-hover table-user">
				<thead>
					<th>Id </th>
					<th>Name </th>
										<th>Email</th>
					<th>Role</th>
					<th>Action</th>
				</thead>
				@foreach($users as $user)
				<tbody>
					<td>{{$user->id}}</td>
					<td>{{$user->name}}</td>
										<td>{{$user->email}}</td>
					<td>{{$user->role->name}}</td>
					<td>
						<a class="btn btn-info btn-sm" href="/user/save/{{$user->id}}">Save</a> ||
						<a class="btn btn-danger btn-sm" href="/user/delete/{{$user->id}}">Delete</a>
					</td>
				</tbody>
				@endforeach
			</table>
		</div>
	</div>
</div>

@endsection