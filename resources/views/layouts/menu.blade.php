<!DOCTYPE html>
<html  class="no-js" lang="en-US">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="SHORTCUT ICON" href="https://demo.My.net/school/v4.2/uploads/images/site.png">
    <title>School Management System  </title>

        <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/animate.css')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/venobox.css')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/slicknav.min.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/responsive.css')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('iniplaylist/iniplaylist.css')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('toastr/toastr.min.css')}}">

    <script type="text/javascript" src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <header id="home">
            <div class="header-top-area hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <span><i class="fa fa-phone"></i>+014011101</span>
                    <a href="#"><i class="fa fa-envelope-o"></i>info@nepgeeks.com</a>
                </div>
                <div class="col-sm-4 text-right">
                    <ul>
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                        <li><a href=""><i class="fa fa-youtube"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
            <nav class="navbar gray-bg mb-0">
        <div class="container">
    
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="lin-menu"></span>
                </button>
                <a class="navbar-brand" href="/"> <span style='color:#86bc42'> My</span><span style='color:#222222'> School</span><span style='color:#D9504E'> Man..</span> </a>
                            </div>
        
                        <div class="collapse navbar-collapse show" id="main-navbar">
                <ul class="main-menu">
                    <li><a href="/">Home</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="/noticeboard">Notice Board</a></li>
                    <li><a href="/teachers">Teachers</a></li>
                    <li><a href="/events">Events</a></li>
                    <li>
                        <a href="#">Media <i class="fa fa-angle-down"></i></a>
                        <ul class="sub-menu">
                            <li><a href="/gallery">Gallery</a></li>
                            <li><a href="/audio-list">Audio List</a></li>
                            <li><a href="/video-list">Video List</a></li></ul>
                            <li><a href="/contact">Contact</a></li>
                            <li><a href="/blog">Blog</a></li>
                            <li><a href="/admission">Admission</a></li>
                            <li><a href="/home">Login</a></li>
                        </li>
                    </ul>
            </div>
        </div>
        <ul class="mobile-menu">
         <li><a href="/">Home</a></li>
         <li><a href="/about">About</a></li>
         <li><a href="/noticeboard">Notice Board</a></li>
         <li><a href="/teachers">Teachers</a></li>
         <li><a href="/events">Events</a></li>
         <li>
            <a href="#">Media <i class="fa fa-angle-down"></i></a>
            <ul class="sub-menu">
                <li><a href="/gallery">Gallery</a></li>
                <li><a href="/audio-list">Audio List</a></li>
                <li><a href="/video-list">Video List</a></li>
            </ul>
            <li><a href="/contact">Contact</a></li>
            <li><a href="/blog">Blog</a></li>
            <li><a href="/admission">Admission</a></li>
            <li><a href="/home">Login</a></li>
            </li>        
        </ul>
    </nav>    
</header>
    @yield('content')


 <footer>
    <div class="footer-top-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="footer-widget">
                        <!-- Start Logo -->
                        <div class="logo footer-logo text-uppercase">
                            <h1>                                                                                                        <a href="/"> <span style='color:#86bc42'> My</span><span style='color:#222222'> School..</span> </a>
                                
                            </h1>
                        </div>
                        <!-- End Logo -->
                        <p>My International School IIS is a fully accredited, independent, international school in Dhaka, Bangladesh.</p>
                        <div class="footer-social">
                            <ul>
                                <li><a href="https://www.facebook.com/My"><i class="fa fa-facebook"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                <li><a href=""><i class="fa fa-youtube"></i></a></li>
                                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1 col-sm-4">
                            <div class="footer-widget">
                                <h2>Information</h2>
                                <ul>                                                                                              <li><a href="/">Home</a></li>Notice Board</a></li>
                                </ul>
                                </div>
                                </div>
                                <div class="col-md-3 col-md-offset-1 col-sm-4">
                                        <div class="footer-widget">
                                            <h2>Usefull links</h2>
                                            <ul>               
                                        </ul>
                                    </div>
                                </div>                                                     
            </div>
        </div>
    </div>
    <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="copyright text-center">
                        Copyright &copy; My School Management System                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>    

    <script src="/js/vendor/jquery-1.12.0.min.js"></script>

    <script src="/js/bootstrap.min.js"></script>

    <script src="/js/owl.carousel.min.js"></script>

    <script src="/js/smoothscroll.js"></script>

    <script src="/js/waypoints.min.js"></script>

    <script src="/js/venobox.min.js"></script>

    <script src="/js/jquery.counterup.min.js"></script>

    <script src="/js/jquery.slicknav.min.js"></script>

    <script src="/js/jquery.easing.1.3.js"></script>

    <script src="/js/plugins.js"></script>

    <script src="/js/main.js"></script>

    <script src="/iniPlaylist/iniplaylist.js"></script>

    <script src="/toastr/toastr.min.js"></script>

    <div id="scroll"><i class="fa fa-angle-up"></i></div>


</body>
</html>