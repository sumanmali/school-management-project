<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <div class="profile-sidebar">
      <div class="profile-userpic">
        <img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
      </div>
      <div class="profile-usertitle">
        <div class="profile-usertitle-name">{{Auth::user()->role->name}} </div>
        <div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="divider"></div>
    <form role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Search">
      </div>
    </form>
    <ul class="nav menu">
      <li><a href="/home"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
@if(Auth::user()->role_id==1)
      <li><a href="/teacher"><em class="fa fa-calendar">&nbsp;</em> Teacher</a></li>
      <li><a href="#"><em class="fa fa-bar-chart">&nbsp;</em> Student</a></li>
      <li><a href="#"><em class="fa fa-toggle-off">&nbsp;</em> Staff</a></li>
      <li><a href="#"><em class="fa fa-clone">&nbsp;</em>Parent</a></li>
           <li><a href="/subject"><em class="fa fa-clone">&nbsp;</em>Subject</a></li>
      <li><a href="#"><em class="fa fa-clone">&nbsp;</em>Attendence</a></li>

      <li><a href="/user"><em class="fa fa-clone">&nbsp;</em>User</a></li>
      <li class="parent "><a data-toggle="collapse" href="/home/slider/index">
        <i class="fa fa-sliders" aria-hidden="true"></i> Slider <span data-toggle="collapse"  class="icon pull-right"></span>
        </a>
      </li>
      <li class="parent "><a data-toggle="collapse" href="/home/work/index">
       <i class="fa fa-film"></i> Work <span data-toggle="collapse"  class="icon pull-right"></span>
        </a>
      </li>

@elseif(Auth::user()->role_id==2)
      <li><a href="#"><em class="fa fa-bar-chart">&nbsp;</em> Student</a></li>
      <li><a href="#"><em class="fa fa-clone">&nbsp;</em>Parent</a></li>
@elseif(Auth::user()->role_id==3)
      <li><a href="#"><em class="fa fa-clone">&nbsp;</em>Subject</a></li>
      <li><a href="#"><em class="fa fa-clone">&nbsp;</em>Attendence</a></li>
@else
@endif
        <li> <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <i class="fa fa-circle-o text-red"></i>
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>

      
     
    </ul>
  </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script>
  jQuery(function($) {
     var path = window.location.href; 
     $('li a').each(function() {
      if (this.href === path) {
       $(this).addClass('active');
      }
     });
    });
</script>