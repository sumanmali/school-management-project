 @extends('layouts.menu')

@section('content')
<!-- bradcame area  -->
 <div class="bradcam-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title white-title bradcam-title text-uppercase text-center">
                   <h2> Gallery </h2>
                   <span class="star"></span>
                   <span class="star"></span>
                   <span class="star"></span>
               </div>
           </div>
           <div class="bradcam-wrap text-center">
            <nav class="bradcam-inner">
              <a class="bradcam-item text-uppercase" href="https://demo.inilabs.net/school/v4.2/frontend/page/home">Home</a>
              <span class="brd-separetor">/</span>
              <span class="bradcam-item active text-uppercase">Gallery</span>
          </nav>
      </div>
  </div>
</div>
</div>
<!-- bradcame area  -->

<section id="about" class="about-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 style="margin-top: 0px; margin-bottom: 25px; font-family: Raleway, sans-serif; font-weight: 700; color: rgb(134, 135, 139);">Student Activities</h2><section id="gallery" class="gallery-area area-padding">
                    <div class="container foredit-container">
                        <div class="row text-center">
                            <div class="gallery-item">

                                <div class="single-gallery">
                                    <img src="/images/Works1548664413.jpg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548664413.jpg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548843765.jpeg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548843765.jpeg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548664413.jpg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548664413.jpg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548666467.jpg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548666467.jpg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548843765.jpeg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548843765.jpeg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548664413.jpg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548664413.jpg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548664413.jpg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548664413.jpg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548664413.jpg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548666467.jpg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>			
                            </div>
                        </div>
                    </div>
                </section><h2 style="margin-top: 0px; margin-bottom: 25px; font-family: Raleway, sans-serif; font-weight: 700; color: rgb(134, 135, 139);">High School Student Activities</h2><section id="gallery" class="gallery-area area-padding">
                    <div class="container foredit-container">
                        <div class="row text-center">
                            <div class="gallery-item">

                                <div class="single-gallery">
                                    <img src="/images/Works1548843765.jpeg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548843765.jpeg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548664413.jpg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548664413.jpg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548664413.jpg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548664413.jpg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                                <div class="single-gallery">
                                    <img src="/images/Works1548666467.jpg" alt="">
                                    <div class="overlay">
                                        <a class="venobox_custom" data-gall="myGellary" href="/images/Works1548666467.jpg"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>			
                            </div>
                        </div>
                    </div>
                </section>&nbsp;                </div> 
            </div>
        </div>
    </section>
    @endsection