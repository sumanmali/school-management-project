@extends('layouts.menu')

@section('content')
<!-- bradcame area  -->
    <div class="bradcam-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
    				<div class="section-title white-title bradcam-title text-uppercase text-center">
    					<h2> Teachers </h2>
                        <span class="star"></span>
                        <span class="star"></span>
                        <span class="star"></span>
    				</div>
    			</div>
                <div class="bradcam-wrap text-center">
                    <nav class="bradcam-inner">
                      <a class="bradcam-item text-uppercase" href="https://demo.inilabs.net/school/v4.2/frontend/page/home">Home</a>
                      <span class="brd-separetor">/</span>
                      <span class="bradcam-item active text-uppercase">Teachers</span>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcame area  -->
    
	
    <section id="our-teacher" class="our-teacher-area area-padding text-center gray-bg">
        <div class="container">
            <div class="row">
            	                    		                <div class="col-md-3 col-sm-4">

                            <div class="teacher-list">
                                <div class="teacher__thumb">
                                    <img src="https://demo.inilabs.net/school/v4.2/uploads/images/default.png" alt="">
                                    <div class="teacher__social">
                                                                                    
                                            <a href="https://www.facebook.com/inilabs"><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                                                            </div>
                                </div>
                                <div class="teacher__body">
                                    <h2>Dipok Kumar Haldar <span>Chief Instructor</span></h2>
                                    
                                                                        
                                </div>
                            </div>
		                </div>
                    		                <div class="col-md-3 col-sm-4">

                            <div class="teacher-list">
                                <div class="teacher__thumb">
                                    <img src="https://demo.inilabs.net/school/v4.2/uploads/images/default.png" alt="">
                                    <div class="teacher__social">
                                                                                    
                                            <a href="https://www.facebook.com/inilabs"><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                                                            </div>
                                </div>
                                <div class="teacher__body">
                                    <h2>Md Rid Islam <span>Instructor</span></h2>
                                    
                                                                        
                                </div>
                            </div>
		                </div>
                    		                <div class="col-md-3 col-sm-4">

                            <div class="teacher-list">
                                <div class="teacher__thumb">
                                    <img src="https://demo.inilabs.net/school/v4.2/uploads/images/default.png" alt="">
                                    <div class="teacher__social">
                                                                                    
                                            <a href="https://www.facebook.com/inilabs"><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                                                            </div>
                                </div>
                                <div class="teacher__body">
                                    <h2>Md Rostom Ali <span>Instructor</span></h2>
                                    
                                                                        
                                </div>
                            </div>
		                </div>
                    		                <div class="col-md-3 col-sm-4">

                            <div class="teacher-list">
                                <div class="teacher__thumb">
                                    <img src="https://demo.inilabs.net/school/v4.2/uploads/images/default.png" alt="">
                                    <div class="teacher__social">
                                                                                    
                                            <a href="https://www.facebook.com/inilabs"><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                                                            </div>
                                </div>
                                <div class="teacher__body">
                                    <h2>Riad Hossain <span>Instructor</span></h2>
                                    
                                                                        
                                </div>
                            </div>
		                </div>
                    		                <div class="col-md-3 col-sm-4">

                            <div class="teacher-list">
                                <div class="teacher__thumb">
                                    <img src="https://demo.inilabs.net/school/v4.2/uploads/images/default.png" alt="">
                                    <div class="teacher__social">
                                                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                                                            </div>
                                </div>
                                <div class="teacher__body">
                                    <h2>Shanon Bepari <span>Instructor</span></h2>
                                    
                                                                        
                                </div>
                            </div>
		                </div>
                                                </div>
        </div>
    </section>

    <!-- Start About Content -->
    <section id="about" class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-about">
                        <p>  </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Close About Content -->
@endsection