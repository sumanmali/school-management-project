@extends('layouts.menu')

@section('content')


<div id="main-slider" class="slider-area">
    @foreach($sliders as $slider)
    <div class="single-slide">
        <img src="/images/{{$slider->image}}" alt="$slider->name">
        <div class="banner-overlay">
            <div class="container">
                <div class="caption style-2">
                    <h2><span>{{$slider->name}}</span></h2>
                    <p>{{$slider->description}}</p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

         @foreach($works as $work)
<section id="about" class="about-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title text-uppercase text-center">
                    <h2>{{$work->name}}</h2>
                    <span class="star"></span>
                    <span class="star"></span>
                    <span class="star"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-push-6">
                <div class="content-img">
                    <img src="/images/{{$work->image}}" />
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="about-content">
                   <p>{{$work->description}}</p>
                </div>
            </div>
        </div>
    </div>
</section>
    @endforeach
@endsection