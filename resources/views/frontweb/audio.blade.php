 @extends('layouts.menu')

@section('content')
<div class="bradcam-area area-padding">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="section-title white-title bradcam-title text-uppercase text-center">
					<h2> Audio List </h2>
					<span class="star"></span>
					<span class="star"></span>
					<span class="star"></span>
				</div>
			</div>
			<div class="bradcam-wrap text-center">
				<nav class="bradcam-inner">
					<a class="bradcam-item text-uppercase" href="https://demo.inilabs.net/school/v4.2/frontend/page/home">Home</a>
					<span class="brd-separetor">/</span>
					<span class="bradcam-item active text-uppercase">Audio List</span>
				</nav>
			</div>
		</div>
	</div>
</div>

<section id="about" class="about-area area-padding">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 style="margin-top: 0px; margin-bottom: 25px; font-family: Raleway, sans-serif; font-weight: 700; color: rgb(134, 135, 139);">How to learn git ?</h2><p style="margin-top: 0px; margin-bottom: 25px; font-family: Raleway, sans-serif; font-weight: 700; color: rgb(134, 135, 139);"><br></p><p></p><div class="item__play__box" id="plybox5071629342525714181">
					<div class="item__header">
						<div class="playing">
							<span class="npAction">
								Now Playing...
							</span>
							<span class="npTitle">
							Introduction of git part -1 What is git and why	    	</span>
						</div>

						<audio preload="" id="audio5071629342525714181" controls="controls" src="http://192.168.0.102/school4.2/uploads/gallery/3abb4421b2f2cc145c165dc8bb35f0e9e207f292d53a63bb9b0847244c71556af8639a654fe0515803b8b81dd8f146fa8f4d6030ab4d050ae5ca3ecad7c2aac6.mp3">Your browser does not support HTML5 Audio!</audio>
					</div>

					<ul class="item__playlist" id="playlist5071629342525714181"><li class="active">
						<div class="playlist__item" onclick="audioPlaylist(this,'plybox5071629342525714181','http://192.168.0.102/school4.2/uploads/gallery/3abb4421b2f2cc145c165dc8bb35f0e9e207f292d53a63bb9b0847244c71556af8639a654fe0515803b8b81dd8f146fa8f4d6030ab4d050ae5ca3ecad7c2aac6.mp3','audio5071629342525714181','playlist5071629342525714181');">
							<div class="item__sl">1.</div>
							<div class="item__title">Introduction of git part -1 What is git and why</div>
							<div class="item__length"></div>
						</div>
					</li>
					<li class="">
						<div class="playlist__item" onclick="audioPlaylist(this,'plybox5071629342525714181','http://192.168.0.102/school4.2/uploads/gallery/c0162267abd10e9c47d559272aa53cb57ad30ddad1d6c281f5403184c7c48dfa6e1c914106d025a8b1977cf33c63cb4117af470cd5269e36622e71e41b19dba4.mp3','audio5071629342525714181','playlist5071629342525714181');">
							<div class="item__sl">2.</div>
							<div class="item__title">Introduction of git part 2 - initialize commit diff and log</div>
							<div class="item__length"></div>
						</div>
					</li>
					<li class="">
						<div class="playlist__item" onclick="audioPlaylist(this,'plybox5071629342525714181','http://192.168.0.102/school4.2/uploads/gallery/65a78eacd27815d137ac12c618ba2ce9c5ae6eddcd19c543db764d160e205c20ee64a0e46669346719cb88ee7dc848928489f4a4a43d8e9af3d153322ab5cf41.mp3','audio5071629342525714181','playlist5071629342525714181');">
							<div class="item__sl">3.</div>
							<div class="item__title">Introduction of git part 3 - back to previous commit reset..</div>
							<div class="item__length"></div>
						</div>
					</li>
				</ul>
			</div>&nbsp;<p></p>				</div> 
		</div>
	</div>
</section>
@endsection