@extends('layouts.menu')

@section('content')
    <!-- bradcame area  -->
    <div class="bradcam-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title white-title bradcam-title text-uppercase text-center">
                        <h2> About </h2>
                        <span class="star"></span>
                        <span class="star"></span>
                        <span class="star"></span>
                    </div>
                </div>
                <div class="bradcam-wrap text-center">
                    <nav class="bradcam-inner">
                      <a class="bradcam-item text-uppercase" href="https://demo.My.net/school/v4.2/frontend/page/home">Home</a>
                      <span class="brd-separetor">/</span>
                      <span class="bradcam-item active text-uppercase">About</span>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcame area  -->

    <section id="about" class="about-area area-padding">
        <div class="container">
            <div class="row">
                                    <div class="col-md-6 col-md-push-6">
                        <div class="content-img">
                            <img src="/images/weall.png" alt="$featured_image->file_alt_text" />
                        </div>
                    </div>
                    <div class="col-md-6 col-md-pull-6">
                        <div class="about-content">
                            <p> <p><span style="color: rgb(95, 95, 95); font-family: arial;">Cambridge International School My is a fully accredited, independent, international school in Dhaka, Bangladesh. Serving families from&nbsp;</span><span style="color: rgb(95, 95, 95); font-family: arial;">Dhaka</span><span style="color: rgb(95, 95, 95); font-family: arial;">’s local and international communities, we successfully deliver a rigorous My curriculum program for students from Early Childhood through Grade 12.</span><br style="color: rgb(95, 95, 95); font-family: arial;"><br style="color: rgb(95, 95, 95); font-family: arial;"><span style="color: rgb(95, 95, 95); font-family: arial;">Our new campus in&nbsp;</span><span style="color: rgb(95, 95, 95); font-family: arial;">Dhaka</span><span style="color: rgb(95, 95, 95); font-family: arial;">&nbsp;is located in the prestigious and residentially exclusive&nbsp;</span><span style="color: rgb(95, 95, 95); font-family: arial;">Dhaka</span><span style="color: rgb(95, 95, 95); font-family: arial;">&nbsp;district, is home to more than 700 students. Recognized for the strong sense of community and connectedness amongst students, teachers and families alike,&nbsp;</span><span style="color: rgb(95, 95, 95); font-family: arial;">My</span><span style="color: rgb(95, 95, 95); font-family: arial;">&nbsp;is proud of the unique and enduring sense of belonging it promotes.</span><br style="color: rgb(95, 95, 95); font-family: arial;"><br style="color: rgb(95, 95, 95); font-family: arial;"><span style="color: rgb(95, 95, 95); font-family: arial;">My</span><span style="color: rgb(95, 95, 95); font-family: arial;">&nbsp;is committed to providing an educational program that is well-balanced, student-centered and appropriately challenging for all.</span><br style="color: rgb(95, 95, 95); font-family: arial;"><br style="color: rgb(95, 95, 95); font-family: arial;"><span style="color: rgb(95, 95, 95); font-family: arial;">We achieve these goals through a challenging academic program enriched by a broad and highly diverse extra-curricular program, vibrant visual and performing arts, an extensive sports program at both intra-mural and varsity levels, unique experiential learning opportunities, challenging leadership programs, dedicated community service, and a schoolwide commitment to supporting student success.</span><br></p> </p>
                        </div>
                    </div>
                            </div>
        </div>
    </section>

@endsection