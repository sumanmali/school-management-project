@extends('layouts.menu')

@section('content')
    <!-- bradcame area  -->
    <div class="bradcam-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
    				<div class="section-title white-title bradcam-title text-uppercase text-center">
    					<h2> Notice Board </h2>
                        <span class="star"></span>
                        <span class="star"></span>
                        <span class="star"></span>
    				</div>
    			</div>
                <div class="bradcam-wrap text-center">
                    <nav class="bradcam-inner">
                      <a class="bradcam-item text-uppercase" href="https://demo.My.net/school/v4.2/frontend/page/home">Home</a>
                      <span class="brd-separetor">/</span>
                      <span class="bradcam-item active text-uppercase">Notice Board</span>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcame area  -->
    
    <section id="notice" class="notice-area area-padding gray-bg">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-4 col-sm-6">
                                <div class="single-notice">
                                    <div class="notice-content">
                                        <h3><a href="https://demo.My.net/school/v4.2/frontend/notice/5">First Semester Exam</a></h3>
                                        <div class="notice-meta">
                                            <span class="published-date">
                                                <i class="fa fa-calendar"></i>
                                                01 Apr 2019                                            </span>
                                        </div>
                                        <p><span xss=removed>Your first semester Exam will held on 19-04-2018.</span></p>
                                        <a href="https://demo.My.net/school/v4.2/frontend/notice/5" class="read-more-btn">read more <i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                          <div class="col-md-4 col-sm-6">
                                <div class="single-notice">
                                    <div class="notice-content">
                                        <h3><a href="https://demo.My.net/school/v4.2/frontend/notice/4">Second Semester Exam</a></h3>
                                        <div class="notice-meta">
                                            <span class="published-date">
                                                <i class="fa fa-calendar"></i>
                                                02 May 2019                                            </span>
                                        </div>
                                        <p><span xss=removed>Your second semester exam will held on 30-08-2018.Please be prepare for your exam</span></p>
                                        <a href="https://demo.My.net/school/v4.2/frontend/notice/4" class="read-more-btn">read more <i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                           <div class="col-md-4 col-sm-6">
                                <div class="single-notice">
                                    <div class="notice-content">
                                        <h3><a href="https://demo.My.net/school/v4.2/frontend/notice/3">Annual Sports Day</a></h3>
                                        <div class="notice-meta">
                                            <span class="published-date">
                                                <i class="fa fa-calendar"></i>
                                                20 May 2019                                            </span>
                                        </div>
                                        <p><span xss=removed>In your school campus on 1-03-2018 will held a program of annual sports day.You are all invited.</span></p>
                                        <a href="https://demo.My.net/school/v4.2/frontend/notice/3" class="read-more-btn">read more <i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="single-notice">
                                    <div class="notice-content">
                                        <h3><a href="https://demo.My.net/school/v4.2/frontend/notice/2">Programming Contest</a></h3>
                                        <div class="notice-meta">
                                            <span class="published-date">
                                                <i class="fa fa-calendar"></i>
                                                30 Apr 2019                                            </span>
                                        </div>
                                        <p><span xss=removed>On 20-11-2018 will held a programming contest in school campus.Registration start from today. </span><br></p>
                                        <a href="https://demo.My.net/school/v4.2/frontend/notice/2" class="read-more-btn">read more <i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="single-notice">
                                    <div class="notice-content">
                                        <h3><a href="https://demo.My.net/school/v4.2/frontend/notice/1">Prize Giving ceremony (Best Student)</a></h3>
                                        <div class="notice-meta">
                                            <span class="published-date">
                                                <i class="fa fa-calendar"></i>
                                                20 Mar 2019                                            </span>
                                        </div>
                                        <p><span xss=removed>Prize giving ceremony will held on 03-01-2019.Best Student List published In your Website.</span></p>
                                        <a href="https://demo.My.net/school/v4.2/frontend/notice/1" class="read-more-btn">read more <i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                             </div>
        </div>
    </section>
    
    <!-- Start About Content -->
    <section id="about" class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-about">
                        <p>  </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection