@extends('layouts.menu')

@section('content')
<div class="bradcam-area area-padding">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="section-title white-title bradcam-title text-uppercase text-center">
					<h2> Video List </h2>
					<span class="star"></span>
					<span class="star"></span>
					<span class="star"></span>
				</div>
			</div>
			<div class="bradcam-wrap text-center">
				<nav class="bradcam-inner">
					<a class="bradcam-item text-uppercase" href="https://demo.inilabs.net/school/v4.2/frontend/page/home">Home</a>
					<span class="brd-separetor">/</span>
					<span class="bradcam-item active text-uppercase">Video List</span>
				</nav>
			</div>
		</div>
	</div>
</div>

<section id="about" class="about-area area-padding">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p></p><h2 style="margin-top: 0px; margin-bottom: 25px; font-family: Raleway, sans-serif; font-weight: 700; color: rgb(134, 135, 139);">Biology</h2>
				<div class="item__play__box" id="playbox3156248941267446611">
					<div class="item__header">
						<video preload="" id="video3156248941267446611" controls="controls" src="https://www.youtube.com/watch?v=1bvYHkQxWmg&list=RD1bvYHkQxWmg&start_radio=1">
							Your browser does not support the video tag.
						</video>
					</div>
					

					<ul class="item__playlist" id="playlist3156248941267446611">
						
						<li class="">
							<div class="playlist__item" onclick="videoPlaylist(this, 'playbox3156248941267446611','https://www.youtube.com/watch?v=1bvYHkQxWmg&list=RD1bvYHkQxWmg&start_radio=1','video3156248941267446611','playlist3156248941267446611');">
								<div class="item__sl">1.</div>
								<div class="item__title">Ape Clarification</div>
								<div class="item__length"></div>
							</div>
						</li>
						<li class="">
							<div class="playlist__item" onclick="videoPlaylist(this, 'playbox3156248941267446611','http://192.168.0.102/school4.2/uploads/gallery/057032d8ff2730d14d89a64d9cd02ae71ff13ab649c365b9f439d8c9894846b5d72a886d6fecf83b3300f4ccc2d7a3b5e07579900897b46f09c12bce7a0d9112.mp4','video3156248941267446611','playlist3156248941267446611');">
								<div class="item__sl">2.</div>
								<div class="item__title">Intelligent Design and Evolution</div>
								<div class="item__length"></div>
							</div>
						</li>
						<li class="active">
							<div class="playlist__item" onclick="videoPlaylist(this, 'playbox3156248941267446611','http://192.168.0.102/school4.2/uploads/gallery/8ccce882ddb92a5a17635f731f1040c301002c9c3e66fc7fbe314bc7d978d801a2c8850bfdadb15f105f3def3843b4294e459221a227e7d901b482af9c3f4a57.mp4','video3156248941267446611','playlist3156248941267446611');">
								<div class="item__sl">3.</div>
								<div class="item__title">Introduction to Evolution and Natural Selection</div>
								<div class="item__length"></div>
							</div>
						</li>
					</ul>
				</div>&nbsp;<p></p><h2 style="margin-top: 0px; margin-bottom: 25px; font-family: Raleway, sans-serif; font-weight: 700; color: rgb(134, 135, 139);">Phisics</h2>
				<div class="item__play__box" id="playbox5308155311346410970">
					<div class="item__header">
						<video preload="" id="video5308155311346410970" controls="controls" src="http://192.168.0.102/school4.2/uploads/gallery/4fbe01f29e38352db98e43bbdcc9e2c5dbb6af2452015d475974f3e4f3034a0a3d7b5127275361b29536664d985b3008f0c10ad581ae06e551968d3a944fbc7e.mp4">
							Your browser does not support the video tag.
						</video>
					</div>
					

					<ul class="item__playlist" id="playlist5308155311346410970">
						
						<li class="active">
							<div class="playlist__item" onclick="videoPlaylist(this, 'playbox5308155311346410970','http://192.168.0.102/school4.2/uploads/gallery/4fbe01f29e38352db98e43bbdcc9e2c5dbb6af2452015d475974f3e4f3034a0a3d7b5127275361b29536664d985b3008f0c10ad581ae06e551968d3a944fbc7e.mp4','video5308155311346410970','playlist5308155311346410970');">
								<div class="item__sl">1.</div>
								<div class="item__title">Solving for time _ One-dimensional motion _ Physics _ Khan..</div>
								<div class="item__length"></div>
							</div>
						</li>
						<li class="">
							<div class="playlist__item" onclick="videoPlaylist(this, 'playbox5308155311346410970','http://192.168.0.102/school4.2/uploads/gallery/07e89a4b82ad1b83ce49e5a931766c8447c4a78fa0d3e436cb1443ac7c6d99305dd1865f623e959163f6aed2345c5ab5e49d6d27e63d0f767a3d8d88cefcaec1.mp4','video5308155311346410970','playlist5308155311346410970');">
								<div class="item__sl">2.</div>
								<div class="item__title">Intro to vectors & scalars _ One-dimensional motion _ ..</div>
								<div class="item__length"></div>
							</div>
						</li>
						<li class="">
							<div class="playlist__item" onclick="videoPlaylist(this, 'playbox5308155311346410970','http://192.168.0.102/school4.2/uploads/gallery/c7f5c4d8116f10792c5ac7ad1344c20df53db6a1e1b428dbaf67815dbc98fe954eb823e4a63fc360604acacd2ee0c0dc2b62dd2ceb9807df2ed53b73a7d49175.mp4','video5308155311346410970','playlist5308155311346410970');">
								<div class="item__sl">3.</div>
								<div class="item__title">Calculating average velocity or speed _ One-dimensional mo..</div>
								<div class="item__length"></div>
							</div>
						</li>
					</ul>
				</div>&nbsp;<p></p>				</div> 
			</div>
		</div>
	</section>
	@endsection