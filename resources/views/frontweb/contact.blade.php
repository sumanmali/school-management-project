@extends('layouts.menu')

@section('content')
<!-- bradcame area  -->
    <div class="bradcam-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title white-title bradcam-title text-uppercase text-center">
                        <h2> Contact </h2>
                        <span class="star"></span>
                        <span class="star"></span>
                        <span class="star"></span>
                    </div>
                </div>
                <div class="bradcam-wrap text-center">
                    <nav class="bradcam-inner">
                      <a class="bradcam-item text-uppercase" href="https://demo.inilabs.net/school/v4.2/frontend/page/home">Home</a>
                      <span class="brd-separetor">/</span>
                      <span class="bradcam-item active text-uppercase">Contact</span>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcame area  -->


    <!--Start Our Contact Us Area -->
    <div id="contact" class="our-contact-us-area area-padding">
        <div class="container white-bg">
            <div class="row">
                <!--COMPANY INFORMATION ITEM-->
                <div class="col-sm-4">
                    <div class="information-box">
                        <div class="company-info">
                            <div class="info-icon">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="info-text">
                                <p>Dhaka</p>
                            </div>
                        </div>
                        <div class="company-info">
                            <div class="info-icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="info-text">
                                <p>+8801728660964</p>
                            </div>
                        </div>
                        <div class="company-info">
                            <div class="info-icon">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <div class="info-text">
                                <p>dipokh@yahoo.com</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--SEND MESSSAGE  ITEM-->
                <div class="col-sm-8">
                    <div class="send-message">
                        <div class="message-field">
                            <form id="contact-form" action="mail.php" method="post">
                                <div class="input-box">
                                    <input id="send-email-name" type="text" name="name" placeholder="Your Name">
                                </div>
                                <div class="input-box">
                                    <input id="send-email-email" type="email" name="email" placeholder="Your Email">
                                </div>
                                <div class="input-box">
                                    <input id="send-email-subject" type="text" name="sub" placeholder="Subject">
                                </div>
                                <div class="input-box">
                                    <textarea name="message" id="send-email-message" maxlength="550" placeholder="Message..."></textarea>
                                    <h5>
                                        <span id="counter__length"></span>
                                        characters remain
                                    </h5>
                                </div>
                                <button type="button" name="ok" class="send-btn" id="send-email">Submit</button>
                            </form>
    						<p class="form-messege"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Our Contact Us Area -->

    
    <!-- Start About Content -->
    <section id="about" class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-about">
                        <p>  </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Close About Content -->
@endsection