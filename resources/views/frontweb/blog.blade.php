@extends('layouts.menu')

@section('content')
<!-- bradcame area  -->
<div class="bradcam-area area-padding">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="section-title white-title bradcam-title text-uppercase text-center">
					<h2> Blog </h2>
					<span class="star"></span>
					<span class="star"></span>
					<span class="star"></span>
				</div>
			</div>
			<div class="bradcam-wrap text-center">
				<nav class="bradcam-inner">
					<a class="bradcam-item text-uppercase" href="https://demo.inilabs.net/school/v4.2/frontend/page/home">Home</a>
					<span class="brd-separetor">/</span>
					<span class="bradcam-item active text-uppercase">Blog</span>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- bradcame area  -->


<section id="about" class="about-area area-padding">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-xs-12">
				<div class="blog-section">
					
					<article class="blog">
						<header>
							<a href="https://demo.inilabs.net/school/v4.2/frontend/post/case-studies">
							Case Studies		                    			</a> 
						</header>
						<div>Posted on 27th January, 2019 by  Admin </div>
						<div class="blog-body">
							<a href="https://demo.inilabs.net/school/v4.2/frontend/post/case-studies">
								<img src="https://demo.inilabs.net/school/v4.2/uploads/gallery//5d0de18063bcef21ba080bffea922c8c0e2d50950ff131a5328ba56c06b986fa6f19dddbadc8f64e43cf9e5208d88f732ce9198aafc5c23d7ad4f37d5123848e.jpg">
							</a>
							<p>
								People love hearing success stories and learning about what goes on behind the scenes. Case studies are fairly easy to write and are great for&nbsp;social proof.Look through your customer list and determine which one has been successful in using yo.. <a href="https://demo.inilabs.net/school/v4.2/frontend/post/case-studies"> Read More » </a>
							</p>
						</div>
					</article>
					<hr>
					<article class="blog">
						<header>
							<a href="https://demo.inilabs.net/school/v4.2/frontend/post/latest-industry-news">
							Latest Industry News		                    			</a> 
						</header>
						<div>Posted on 27th January, 2019 by  Admin </div>
						<div class="blog-body">
							<a href="https://demo.inilabs.net/school/v4.2/frontend/post/latest-industry-news">
								<img src="https://demo.inilabs.net/school/v4.2/uploads/gallery//5bd9e39a8fcb19abc45fe6710b80004a07191eae356a4cb7b6b59f45a4f8c9dca190de28316cf65b6da6a2bf1a14577b5c6ba9eeb11fc719542033cb6c04ce88.jpg">
							</a>
							<p>
								You have your morning routine, just like I do. You skim the online news and find the latest trends in your industry. Why not make your morning routine into a blog post?Find few of the most recent events in your industry and blog about them. You can.. <a href="https://demo.inilabs.net/school/v4.2/frontend/post/latest-industry-news"> Read More » </a>
							</p>
						</div>
					</article>
					<hr>
					<article class="blog">
						<header>
							<a href="https://demo.inilabs.net/school/v4.2/frontend/post/tutorials-and-how-to-guides">
							Tutorials And How-to Guides</a> 
						</header>
						<div>Posted on 27th January, 2019 by  Admin </div>
						<div class="blog-body">
							<a href="https://demo.inilabs.net/school/v4.2/frontend/post/tutorials-and-how-to-guides">
								<img src="https://demo.inilabs.net/school/v4.2/uploads/gallery//5b64b3d8d6f2b741ca1f54f761ab54db3eaabfe45d9e837d7775b8019ad7c9db2623ee3f3a5a7cbc462cc4fb469eb76e0f039fbd55b5cd9df262fdf8ddb638ba.jpg">
							</a>
							<p>Tutorials and How-to guides are probably the simplest type of blog post you can work on. They are easy because they involve you talking about things you are already familiar with such as your product or service.The most important thing when it come.. <a href="https://demo.inilabs.net/school/v4.2/frontend/post/tutorials-and-how-to-guides"> Read More » </a></p>
						</div>
					</article>
					<hr>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4">
				<div class="blog-recennt-post">
					<h2><span>Recent</span> Posts</h2> 
					
					<div class="post-title"><a href="https://demo.inilabs.net/school/v4.2/frontend/post/case-studies"> <i class="fa fa-arrow-right"></i> Case Studies</a></div>
					<div class="post-title"><a href="https://demo.inilabs.net/school/v4.2/frontend/post/latest-industry-news"> <i class="fa fa-arrow-right"></i> Latest Industry News</a></div>
					<div class="post-title"><a href="https://demo.inilabs.net/school/v4.2/frontend/post/tutorials-and-how-to-guides"> <i class="fa fa-arrow-right"></i> Tutorials And How-to Guides</a></div>
				</div>

			</div>
		</div>
	</div>
</section>
@endsection