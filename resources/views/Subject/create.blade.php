@extends('layouts.admin')

@section('content')
<div class="container-fluid">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		<div class="col-sm-3 admin_sidebar">
			@include('layouts.sidebar')
		</div>
		<div class="col-sm-9 col-sm-8">
			<form method="post" action="/subject/create">
			@csrf
				<h3>Add New Subject</h3>
				<div class="form-group">
					<input type="hidden" value="{{csrf_token()}}" name="subject"/>
					<label for="subject">Subject Name:</label>
					<input class="form-control" type="text" name="name"/>
					
				</select>
					<input type="hidden" value="{{csrf_token()}}" name="description"/>
					<label for="description">Description:</label>
					<input class="form-control" type="text" name="description"/>
				</div>
				<div class="form-group">
					<button type="submit" class="form-control btn btn-primary btn-block">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection