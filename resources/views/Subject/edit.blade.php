@extends('layouts.admin')

@section('content')
<div class="container-fluid">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		<div class="col-sm-3 admin_sidebar">
			@include('layouts.sidebar')
		</div>
		<div class="col-sm-9 col-sm-8">
			<form method="post" action="/subject/edit/{{$subject->id}}">
			@csrf
				<h3>Edit Subject</h3>
				<div class="form-group">
					<input type="hidden" value="{{csrf_token()}}" name="name"/>
					<label for="name">name:</label>
					<input class="form-control" type="text" name="name" value='{{ $subject->name }}'/>
					<input type="hidden" value="{{csrf_token()}}" name="description"/>
					<label for="description">Description:</label>
					<input class="form-control" type="text" name="description" value='{{$subject->description}}'/>
				</div>
				<div class="form-group">
					<button type="submit" class="form-control btn btn-primary btn-block">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection