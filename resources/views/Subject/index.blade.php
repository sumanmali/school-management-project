@extends('layouts.admin')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3 admin_sidebar">
			@include('layouts.sidebar')
		</div>
		<div class="col-sm-9 col-sm-8">
			<h3>Subject</h3>
			<a class="btn btn-primary" href="{{url('/subject/create')}}">Create subject</a>
			<table class="table table-hover table-teacher">
				<thead>
					<th>id </th>
					<th>name </th>
					<th> description</th>
					<th>Action</th>
				</thead>
				@foreach($subject as $subjects)
				<tbody>
					<td>{{$subjects->id}}</td>
					<td>{{$subjects->name}}</td>
					<td>{{$subjects->description}}</td>
					<td>
						<a class="btn btn-info btn-sm" href="/subject/edit/{{$subjects->id}}">Edit</a><br>
						<a class="btn btn-danger btn-sm" href="/subject/delete/{{$subjects->id}}">Delete</a>
					</td>
				</tbody>
				@endforeach
			</table>
		</div>
	</div>
</div>

@endsection