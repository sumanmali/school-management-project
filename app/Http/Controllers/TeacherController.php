<?php

namespace App\Http\Controllers;

use App\teacher;
use Illuminate\Http\Request;
use App\Subject;
class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacher = teacher::all();
        return view('teacher.index', compact('teacher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $subject = Subject::all();
       return view('teacher.create',compact('subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $this->validate($request, array(
            'name' => 'required|max:30',
            ));
        $teacher = new teacher();
        $teacher->name = $request->input('name');
        $teacher->subject=$request->input('subject');
        if(file_exists($request->image)){
            $filename = 'teacher'.time().'.'.$request->image->getClientOriginalExtension();
            $location = public_path('images/');
            $request->image->move($location, $filename);
            $teacher->image = $filename;
        }
        else{
            $teacher->image = 'https://static.thenounproject.com/png/17241-200.png';
        }
        $teacher->save();
        return redirect('/teacher')->with('Success, ', 'A new teacher is added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(teacher $teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = teacher::findorFail($id);
        return view('teacher.edit', compact('teacher', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $teacher = teacher::find($id);
        $this->validate($request, array(
            'name' => 'required|max:30',
            ));
        $teacher = teacher::where('id', $id)->first();
        $teacher->name = $request->input('name');
          $teacher->subject=$request->input('subject');
          if(file_exists($request->image)){
            $filename = 'teacher'.time().'.'.$request->image->getClientOriginalExtension();
            $location = public_path('images/');
            $request->image->move($location, $filename);
            $teacher->image = $filename;
        }
        else{
            $teacher->image = 'https://static.thenounproject.com/png/17241-200.png';
        }
        $teacher->save();
        return redirect('/teacher')->with('Success, ', 'Teacher is updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = teacher::findorFail($id);
        $teacher->delete();
        return redirect('/teacher')->with('Success', 'Teacher is deleted successfully.');
    }
}
