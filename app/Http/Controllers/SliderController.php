<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = slider::all();
        return view('admin/slider/index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin/slider/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = new Slider();
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $slider->name = $request->name;
        $slider->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "Slider".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('images');
            $request->file('image')->move($location, $image);
            $slider->image = $image;
        }
        else{
            $slider->image = '123.jpg';
        }        
        $slider->save();
        return redirect('/home/slider/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider,$id)
    {
        $slider = slider::findOrFail($id);
        return view('admin/slider/edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider,$id)
    {
        $slider = slider::findOrFail($id);

        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $slider->name = $request->name;
        $slider->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "Slider".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('images');
            $request->file('image')->move($location, $image);
            $slider->image = $image;
        }
        else{
            $slider->image = $slider->image;
        }        
        $slider->save();
        return redirect('/home/slider/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = slider::findOrFail($id) ->delete();
        return back();
    }
}
