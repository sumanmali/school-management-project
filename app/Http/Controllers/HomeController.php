<?php

namespace App\Http\Controllers;
use App\User;
use App\Subject;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

                $users = User::all();
                $subject = Subject::all();
                $teacher = User::all()->where('role_id', 2);
        return view('home', compact('users','subject','teacher'));

        return view('frontweb/home');

    }
}
