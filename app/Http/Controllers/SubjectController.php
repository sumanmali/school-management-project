<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subject = Subject::all();
        return view('subject.index', compact('subject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('subject.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, array(
            'name' => 'required|max:30',
            ));
        $subject = new Subject();
        $subject->name = $request->input('name');
        $subject->description=$request->input('description');
        $subject->save();
        return redirect('/subject')->with('Success, ', 'A new subject is added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $subject = Subject::findorFail($id);
        return view('subject.edit', compact('subject', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = Subject::find($id);
        $this->validate($request, array(
            'name' => 'required|max:30',
            ));
        $subject = Subject::where('id', $id)->first();
        $subject->name = $request->input('name');
          $subject->description=$request->input('description');
        $subject->save();
        return redirect('/subject')->with('Success, ', 'Subject is updated successfully.');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
       $subject = Subject::findorFail($id);
        $subject->delete();
        return redirect('/subject')->with('Success', 'Subject is deleted successfully.');
    }
}
