<?php

namespace App\Http\Controllers;

use App\Work;
use Illuminate\Http\Request;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $works = Work::all();
        return view('admin/work.index', compact('works'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin/work.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $works = new Work();
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $works->name = $request->name;
        $works->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "Works".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('images');
            $request->file('image')->move($location, $image);
            $works->image = $image;
        }
        else{
            $works->image = '123.jpg';
        }        
        $works->save();
        return redirect('/home/work/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function show(Work $work)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function edit(Work $work,$id)
    {
        $works = Work::findOrFail($id);
        return view('admin/work.edit', compact('works'));    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Work $work,$id)
    {
        $works = Work::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $works->name = $request->name;
        $works->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "Works".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('images');
            $request->file('image')->move($location, $image);
            $works->image = $image;
        }
        else{
            $works->image = $works->image;
        }        
        $works->save();
        return redirect('/home/work/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $works = Work::findOrFail($id)->delete();
        return redirect()->back();
    }
}
